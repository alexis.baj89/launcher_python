import socket
import pickle
import requests

HOST = 'localhost'  # The server's hostname or IP address
PORT = 12345  # The port used by the server


class Client:
    def __init__(self) -> None:
        pass

    def send(self, action, parameters):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((HOST, PORT))
            parameters['action'] = action
            data = pickle.dumps(parameters)
            s.sendall(data)
            print(s.getsockname())
            data = s.recv(1024)
        print(f'Received: {data.decode()}')


class ClientHTTP:
    def __init__(self, server, port) -> None:
        self._server = server
        self._port = port

    def send(self, action, parameters):
        url = f'http://{self._server}:{self._port}/{action}'
        payload = {}
        headers = {}

        response = requests.request("GET", url, headers=headers, data=payload)
        return response.content.decode('UTF-8')

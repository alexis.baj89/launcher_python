import socket
import pickle
import time
from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.parse import urlparse, parse_qs
import jsonpickle

HOST = 'localhost'  # Symbolic name meaning all available interfaces
PORT = 8000  # Arbitrary non-privileged port


class Server:
    def __init__(self, menu) -> None:
        self._menu = menu

    def run(self):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.bind((HOST, PORT))
            s.listen(1)
            print(f'Server listening on port {PORT}...')
            while True:
                conn, addr = s.accept()
                with conn:
                    print(f'Connected by {addr}')
                    while True:
                        time.sleep(10)
                        data = conn.recv(1024)
                        if not data:
                            break
                        parameters = pickle.loads(data)
                        result = self._menu._main_entries[parameters['action']](parameters)
                        self._menu._application.save()
                        conn.sendall(result.encode())


def create_handler(menu):
    class GagHTTPHandler(BaseHTTPRequestHandler):
        def __init__(self, *args, **kwargs) -> None:
            print('>>>>>>>>>init gag http handler')
            self._menu = menu
            super().__init__(*args, **kwargs)

        def do_GET(self):
            parsed_path = urlparse(self.path)
            path = parsed_path.path
            query = parse_qs(parsed_path.query)
            response_messsage = ''
            root_path = {

                '/lib/games': self._menu.displayGames,
                '/store/games': self._menu.displayGamesInStore,
                '/store/game/buy': self._menu.buyGame,
                '/store/game': self._menu.detailGame
            }
            parameters = {}
            print(query)
            if 'game' in query:
                game_name = query['game'][0]
                parameters['name'] = game_name
            response_messsage = root_path[path](parameters)

        def do_POST(self):
            parsed_path = urlparse(self.path)
            path = parsed_path.path
            length = int(self.headers['Content-Length'])
            field_data = self.rfile.read(length)
            response_message = 'nothing'
            if self.path == '/store/game':
                game = jsonpickle.decode(field_data)
                pass
            if self.path == '/lib/game/comment':
                pass
            print(self.request)

    return GagHTTPHandler


class ServerHttp:
    def __init__(self, menu) -> None:
        self._menu = menu

    def run(self, server_class=HTTPServer):
        server_address = ('', PORT)
        httpd = server_class(server_address, create_handler(self._menu))
        httpd.serve_forever()

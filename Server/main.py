from Store import Store
from User import User
from menu import Menu
from persist import PersistJson
from application import Application
from Server import ServerHttp

if __name__ == '__main__':
    persist = PersistJson()
    application = persist.load()
    if application == None:
        user = User(name='user1')
        store = Store()
        application = Application(store, user, persist)
    menu = Menu(application)
    server = ServerHttp(menu)
    server.run()
